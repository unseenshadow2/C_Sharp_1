﻿using GameDataStorage;
using RoomManager.Map;
using RoomManager.Map.Descriptions;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MapStartup
{
    public static class StartUp
    {
        private static MapDescriptionsLoader loader = new MapDescriptionsLoader();

        /// <summary>
        /// Handle setting up the map in a single line. Particularly the names and descriptions.
        /// Uses the default map from GameData.
        /// </summary>
        public static void SetupMap()
        {
            try
            {
                SetupMap(GameData.GetMap);
            }
            catch (DirectoryNotFoundException ex)
            {
                File.WriteAllText("Startup.log", "Could not setup map. See other logs for potential issues.");
                throw ex;
            }
        }

        /// <summary>
        /// Handle setting up the map in a single line. Particularly the names and descriptions.
        /// Uses the default map from GameData.
        /// </summary>
        public static void WebSetupMap()
        {
            loader.LoadNameFromString(MapDescriptionsList.descriptionList);
            MapDescriptionSetup.AssignNamesAndDescriptions(loader.GetNames, GameData.GetMap);
        }

        /// <summary>
        /// Handle setting up the map in a single line. Particularly the names and descriptions.
        /// </summary>
        /// <param name="map">The map that we need to setup.</param>
        public static void SetupMap(Map map)
        {
            try
            { 
                loader.LoadNameFromFile("RoomDescriptions.txt");
                MapDescriptionSetup.AssignNamesAndDescriptions(loader.GetNames, map);
            }
            catch (DirectoryNotFoundException ex)
            {
                throw ex;
            }
        }
    }
}
