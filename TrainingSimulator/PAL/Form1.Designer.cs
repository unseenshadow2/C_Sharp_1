﻿namespace PAL
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnNorth = new System.Windows.Forms.Button();
            this.btnWest = new System.Windows.Forms.Button();
            this.btnEast = new System.Windows.Forms.Button();
            this.btnSouth = new System.Windows.Forms.Button();
            this.lblNameDescription = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.sfdLocationFile = new System.Windows.Forms.SaveFileDialog();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnNorth
            // 
            this.btnNorth.Location = new System.Drawing.Point(148, 39);
            this.btnNorth.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnNorth.Name = "btnNorth";
            this.btnNorth.Size = new System.Drawing.Size(100, 28);
            this.btnNorth.TabIndex = 0;
            this.btnNorth.Text = "North";
            this.btnNorth.UseVisualStyleBackColor = true;
            this.btnNorth.Click += new System.EventHandler(this.MoveButton);
            // 
            // btnWest
            // 
            this.btnWest.Location = new System.Drawing.Point(16, 105);
            this.btnWest.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnWest.Name = "btnWest";
            this.btnWest.Size = new System.Drawing.Size(100, 28);
            this.btnWest.TabIndex = 1;
            this.btnWest.Text = "West";
            this.btnWest.UseVisualStyleBackColor = true;
            this.btnWest.Click += new System.EventHandler(this.MoveButton);
            // 
            // btnEast
            // 
            this.btnEast.Location = new System.Drawing.Point(280, 105);
            this.btnEast.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnEast.Name = "btnEast";
            this.btnEast.Size = new System.Drawing.Size(100, 28);
            this.btnEast.TabIndex = 2;
            this.btnEast.Text = "East";
            this.btnEast.UseVisualStyleBackColor = true;
            this.btnEast.Click += new System.EventHandler(this.MoveButton);
            // 
            // btnSouth
            // 
            this.btnSouth.Location = new System.Drawing.Point(148, 170);
            this.btnSouth.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnSouth.Name = "btnSouth";
            this.btnSouth.Size = new System.Drawing.Size(100, 28);
            this.btnSouth.TabIndex = 3;
            this.btnSouth.Text = "South";
            this.btnSouth.UseVisualStyleBackColor = true;
            this.btnSouth.Click += new System.EventHandler(this.MoveButton);
            // 
            // lblNameDescription
            // 
            this.lblNameDescription.AutoSize = true;
            this.lblNameDescription.Location = new System.Drawing.Point(4, 7);
            this.lblNameDescription.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblNameDescription.MaximumSize = new System.Drawing.Size(267, 277);
            this.lblNameDescription.Name = "lblNameDescription";
            this.lblNameDescription.Size = new System.Drawing.Size(83, 34);
            this.lblNameDescription.TabIndex = 4;
            this.lblNameDescription.Text = "Name:\r\nDescription:\r\n";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.lblNameDescription);
            this.panel1.Location = new System.Drawing.Point(388, 39);
            this.panel1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.panel1.MaximumSize = new System.Drawing.Size(467, 985);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(300, 308);
            this.panel1.TabIndex = 5;
            // 
            // sfdLocationFile
            // 
            this.sfdLocationFile.Title = "Location File";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(719, 370);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.btnSouth);
            this.Controls.Add(this.btnEast);
            this.Controls.Add(this.btnWest);
            this.Controls.Add(this.btnNorth);
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "Form1";
            this.Text = "Blade Runner Basement Map";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnNorth;
        private System.Windows.Forms.Button btnWest;
        private System.Windows.Forms.Button btnEast;
        private System.Windows.Forms.Button btnSouth;
        private System.Windows.Forms.Label lblNameDescription;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.SaveFileDialog sfdLocationFile;
    }
}

