﻿using MapStartup;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using RoomManager.Map;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RoomManager.Map.Tests
{
    [TestClass()]
    public class MapTests
    {
        [TestMethod()]
        public void MapConstructorTest_Success()
        {
            Map map = new Map(); // Ensure that maps startup correctly
            StartUp.SetupMap(map);

            Assert.IsNotNull(map.GetRooms[3, 1].GetNorth);

            //Assert.Fail();
        }

        [TestMethod()]
        public void MapConstructorTest_Failure()
        {
            Map map = new Map(); // Ensure that maps startup correctly
            StartUp.SetupMap(map);

            Assert.IsNull(map.GetRooms[3, 1].GetSouth);

            //Assert.Fail();
        }
    }
}