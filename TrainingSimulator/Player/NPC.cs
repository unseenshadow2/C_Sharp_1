﻿using MapMovement;
using RoomManager;
using RoomManager.Map;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Player
{
    class NPC : AI
    {
        // Can we power heal the player
        bool canPowerHeal = true;

        // Handle resetting the power heal
        int lastPowerHeal = 0;
        const int healPause = 5;

        /// <summary>
        /// Move once in the given direction...
        /// </summary>
        /// <param name="direction">The direction to travel.</param>
        public override void Move(Direction direction)
        {
            base.Move(direction);

            if (lastPowerHeal > 0) --lastPowerHeal;
        }

        /// <summary>
        /// Fight the other target...
        /// </summary>
        /// <param name="target">The target to fight.</param>
        public override void Fight(IPlayer target)
        {
            target.SetHealth = target.GetHealth + 1;
        }

        /// <summary>
        /// A more powerful version of the heal that requires time to recharge.
        /// </summary>
        /// <param name="target">The target of the power heal</param>
        public void PowerHeal(IPlayer target)
        {
            if (canPowerHeal)
            {
                target.SetHealth = target.GetHealth + 5;
                canPowerHeal = false;
                lastPowerHeal += healPause;
            }
        }

        // Getters and Setters
        bool GetCanPowerHeal { get { return canPowerHeal; } }

        int GetLastPowerHeal { get { return lastPowerHeal; } }

        int GetHealPause { get { return healPause; } }
    }
}
