﻿using RoomManager;
using RoomManager.Map;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Player
{
    /// <summary>
    /// The basic player interface.
    /// </summary>
    public interface IPlayer
    {
        /// <summary>
        /// Move once in the given direction...
        /// </summary>
        /// <param name="direction">The direction to travel.</param>
        void Move(Direction direction);

        /// <summary>
        /// Fight the other target...
        /// </summary>
        /// <param name="target">The target to fight.</param>
        void Fight(IPlayer target);

        // Getters and Setters
        Room GetLocation { get; }
        Room SetLocation { set; }

        int GetHealth { get; }
        int SetHealth { set; }
    }
}
