﻿using GameDataStorage;
using MapMovement;
using RoomManager;
using RoomManager.Map;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Player
{
    public abstract class Human : IPlayer
    {
        private Room location = GameData.GetRoom;
        private int health = 10;

        /// <summary>
        /// Move once in the given direction...
        /// </summary>
        /// <param name="direction">The direction to travel.</param>
        public virtual void Move(Direction direction)
        {
            MapMove.Move(direction);
            SetLocation = GameData.GetRoom;
        }

        /// <summary>
        /// Fight the other target...
        /// </summary>
        /// <param name="target">The target to fight.</param>
        public virtual void Fight(IPlayer target)
        {
            target.SetHealth = target.GetHealth - 1;
        }

        // Getters and Setters
        public Room GetLocation { get { return location; } }
        public Room SetLocation { set { location = value; } }

        public int GetHealth { get { return health; } }
        public int SetHealth { set { health = value; } }
    }
}
