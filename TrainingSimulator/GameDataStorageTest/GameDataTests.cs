﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace GameDataStorageTest
{
    [TestClass]
    public class GameDataTests
    {
        [TestMethod]
        public void GetRoomTest_Success()
        {
            Assert.IsNotNull(GameDataStorage.GameData.GetRoom);
        }

        [TestMethod]
        public void SetRoomTest_Success()
        {
            RoomManager.Room testRoom = new RoomManager.Room();

            GameDataStorage.GameData.SetRoom = testRoom;

            Assert.AreEqual(testRoom, GameDataStorage.GameData.GetRoom);
        }
    }
}
