
######################
#### Introduction ####
######################

Project: Desktop Application with Control Structures
Author: Anthony Parsch
Purpose:
	To provide an enjoyable game written in C# that meets the requirements of the
	assignment with the same name.

######################
#### Instructions ####
######################

1. Open AdventureMap.sln in Visual Studio 2017
2. Right click the solution "PAL"
3. Click "Set as StartUp Project"
4. Click "Start" button

###########################
#### Other Information ####
###########################

The unit test with Pass/Fail for this project is located in "RoomManagerTests.MapTests.cs".

#####################
#### Conventions ####
#####################
When checking how large a class is, please hide (click the plus button) summary comments.

Due to personal preference, I'll do getters and setters as properties (variables that hide a set/get function)
	with the syntax of YYYXXX where YYY is Get or Set and XXX is the name of the variable.

	In order to make the code cleaner, all getters and setters that contain no special logic or validation will
	be in the "// Getters and Setters" section of each class and condensed to a single line each. If they contain
	said logic/validation, then they will be included in the rest of their class.

	I will not be counting any code in the "// Getters and Setters" section towards the size of their class. Getters
	and setters with special logic (and thus not included in said section) will be counted towards the size of their
	class.