﻿using MapStartup;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using MapMovement;
using RoomManager.Map;

namespace WebApplicationUI
{
    public partial class _Default : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            StartUp.WebSetupMap();
            UpdateControls();
        }

        protected void moveButton_Click(object sender, EventArgs e)
        {
            Button button = sender as Button; // Make sure we can access the name of the sender

            // Move in the given direction
            if (button.ID == "btnNorth") MapMove.Move(Direction.North);
            else if (button.ID == "btnSouth") MapMove.Move(Direction.South);
            else if (button.ID == "btnEast") MapMove.Move(Direction.East);
            else if (button.ID == "btnWest") MapMove.Move(Direction.West);
            else return; // Not an expected input, do nothing

            UpdateControls(); // Update the controls
            DisplaySuccess(); // Show success if we have achieved it
        }

        private void UpdateControls()
        {
            // Get where we can move
            AvailableDirections directions = AvailableDirections.GetAvailableDirections();

            // Show where we can move
            btnNorth.Enabled = directions.GetNorth;
            btnSouth.Enabled = directions.GetSouth;
            btnEast.Enabled = directions.GetEast;
            btnWest.Enabled = directions.GetWest;

            // Update our string display
            lblNameDescription.Text = GetRoomString.GetString();
        }

        private void DisplaySuccess()
        {
            // If we have visited all rooms, alert the agent
            if (CheckKnownLocations.HasVisitedAllLocations())
            {
                //MessageBox.Show("I hope you found something in there, agent, because you have explored every room.", "Congratulations");
                //Server.Transfer("Victory.aspx", false);
                Response.Redirect("Victory.aspx");
            }
        }
    }
}