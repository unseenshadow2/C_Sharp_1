﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="WebApplicationUI._Default" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <div style="width:100%;" class="container-fluid">
        <div class="row">
            <div class="col-md-4" style="text-align:center;">
            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                <ContentTemplate>
                    <!-- Button Table -->
                    <table id="button_table">
                        <tr>
                            <td></td>
                            <td><asp:Button ID="btnNorth" runat="server" Text="North" OnClick="moveButton_Click" /></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td><asp:Button ID="btnWest" runat="server" Text="West" OnClick="moveButton_Click" /></td>
                            <td></td>
                            <td><asp:Button ID="btnEast" runat="server" Text="East" OnClick="moveButton_Click" /></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td><asp:Button ID="btnSouth" runat="server" Text="South" OnClick="moveButton_Click" /></td>
                            <td></td>
                        </tr>
                    </table>
                </ContentTemplate>
            </asp:UpdatePanel>
            </div>

            <div class="col-md-6" style="text-align:center;">
            <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                <ContentTemplate>
                    <!-- Description box -->
                    <asp:TextBox ID="lblNameDescription" runat="server" Enabled="False" Height="125px" ReadOnly="True" Width="300px" TextMode="MultiLine">Name:
Description:</asp:TextBox>
                </ContentTemplate>
            </asp:UpdatePanel>
            </div>
        </div>
    </div>
</asp:Content>
