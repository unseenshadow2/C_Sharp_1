﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;

namespace RoomManager.Map.Descriptions
{
    /// <summary>
    /// Handles loading a file of names and descriptions into an array of strings for
    /// use with the MapDescriptionSetup class.
    /// </summary>
    public class MapDescriptionsLoader
    {
        // The array of names and descriptions
        private String[,,] names = new string[4,3,2];

        // How we split up our names and descriptions. {row, column}, {name}, {description}
        private Regex namesRegex = new Regex(@"{(\d+), (\d+)}, {(.+)}, {(.+)}");

        /// <summary>
        /// Loads the "names" variable with a list of names from the file with the matching file name. 
        /// The file should contain the data in the given format: "{row, column}, {name}, {description}"
        /// </summary>
        /// <param name="filename">The name of the file to take names from.</param>
        public void LoadNameFromFile(String filename)
        {
            // Pull in the file that will be turned 
            string nameList = File.ReadAllText(filename);

            ProcessStringToNames(nameList);
        }

        /// <summary>
        /// Takes the string of a name/description file and handles placing the values into "names."
        /// </summary>
        /// <param name="nameList">The string of the name/description file.</param>
        private void ProcessStringToNames(String nameList)
        {
            MatchCollection matches = namesRegex.Matches(nameList);
            if (matches.Count > 0)
            {
                foreach (Match match in matches)
                {
                    ProcessMatch(match);
                }                    
            }
        }

        /// <summary>
        /// Take a match from "namesRegex" and place the data into "names."
        /// </summary>
        /// <param name="match">The match from namesRegex.</param>
        private void ProcessMatch(Match match)
        {
            int row = Int32.Parse(match.Groups[1].Value);
            int col = Int32.Parse(match.Groups[2].Value);
            names[row, col, 0] = match.Groups[3].Value;
            names[row, col, 1] = match.Groups[4].Value;
        }

        // Getters and Setters
        public String[,,] GetNames { get { return names; } }
    }
}
