﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RoomManager
{
    /// <summary>
    /// This class represents an item.
    /// </summary>
    public abstract class Item
    {
        private String name = null; // The name of the item
        private String description = null; // The description of the item

        /// <summary>
        /// This function is run when the item is picked up.
        /// Actions that happen during pickup should be run in this function.
        /// </summary>
        public abstract void PickupItem();

        // Getters and Setters
        public String GetName { get { return name; } }
        public String SetName { set { name = value; } }

        public String GetDescription { get { return description; } }
        public String SetDescription { set { description = value; } }
    }
}
