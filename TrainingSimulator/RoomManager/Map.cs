﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RoomManager.Map
{
    /// <summary>
    /// An enum to represent direction.
    /// </summary>
    public enum Direction { North, South, East, West };

    /// <summary>
    /// This class is a data storage class. It handles storing the map that holds the rooms.
    /// </summary>
    public class Map
    {
        // This is the actual map of rooms, with the player theoretically starting at [0,1]
        private Room[,] rooms =
        {
            {null, new Room(), null },
            {new Room(), new Room(), new Room() },
            {new Room(), new Room(), new Room() },
            {null, new Room(), null }
        };

        public Map()
        {
            for (int row = 0; row < rooms.GetLength(0); row++)
            {
                for (int col = 0; col < rooms.GetLength(1); col++)
                {
                    // Set the attached rooms with position protection
                    rooms[row, col].SetAttachedRooms(GetRoomAt(row + 1, col), GetRoomAt(row - 1, col), GetRoomAt(row, col + 1), GetRoomAt(row, col - 1));
                }
            }
        }

        private Room GetRoomAt(int row, int col)
        {
            // Validate position
            if ((row >= 0) && (row < rooms.GetLength(0)) && (col >= 0) && (col < rooms.GetLength(1)))
            {
                return rooms[row, col];
            }
            else return null;
        }

        // Getters and Setters
        public Room[,] GetRooms { get { return rooms; } }
    }
}
