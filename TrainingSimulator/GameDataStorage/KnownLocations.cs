﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameDataStorage
{
    public class KnownLocations
    {
        // The rooms we have visited, only the starting room by default
        private bool[,] roomsVisited =
        {
            {false, true, false },
            {false, false, false },
            {false, false, false },
            {false, false, false }
        };

        // Our location
        private int row = 0;
        private int col = 1;

        // Getters and Setters
        public bool[,] GetRoomsVisited { get { return roomsVisited; } }

        public int GetRow { get { return row; } }
        public int SetRow { set { row = value; } }

        public int GetCol { get { return col; } }
        public int SetCol { set { col = value; } }
    }
}
