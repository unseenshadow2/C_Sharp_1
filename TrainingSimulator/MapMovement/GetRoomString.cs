﻿using GameDataStorage;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MapMovement
{
    /// <summary>
    /// This class exists to generate a string for the current room.
    /// </summary>
    public static class GetRoomString
    {
        /// <summary>
        /// Get a string that represents the current room.
        /// </summary>
        /// <returns>The string that represents the current room</returns>
        public static string GetString()
        {
            StringBuilder builder = new StringBuilder();

            builder.Append("Name: ");
            builder.Append(GameData.GetRoom.GetName + Environment.NewLine);
            builder.Append("Description: " + Environment.NewLine);
            builder.Append(GameData.GetRoom.GetDescription);

            return builder.ToString();
        }
    }
}
