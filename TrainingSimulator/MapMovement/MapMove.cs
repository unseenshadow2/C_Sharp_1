﻿using GameDataStorage;
using RoomManager.Map;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MapMovement
{
    /// <summary>
    /// This static class handles moving within the map.
    /// </summary>
    public static class MapMove
    {
        /// <summary>
        /// Check if we can move in a direction and move if we can.
        /// </summary>
        /// <param name="direction">The direction we are traveling in.</param>
        /// <returns>Returns whether we moved.</returns>
        public static bool Move(Direction direction)
        {
            bool toReturn = DirectionCheck.CheckDirection(direction);

            if (toReturn) ChangeLocation(direction);

            return toReturn;
        }

        /// <summary>
        /// Change the room to the room in the direction given.
        /// </summary>
        /// <param name="direction">The direction we are traveling in.</param>
        private static void ChangeLocation(Direction direction)
        {
            switch (direction)
            {
                case Direction.North:
                    GameData.SetRoom = GameData.GetRoom.GetNorth;
                    break;
                case Direction.South:
                    GameData.SetRoom = GameData.GetRoom.GetSouth;
                    break;
                case Direction.East:
                    GameData.SetRoom = GameData.GetRoom.GetEast;
                    break;
                case Direction.West:
                    GameData.SetRoom = GameData.GetRoom.GetWest;
                    break;
            }

            // Make sure we keep track of if we have visited all rooms.
            UpdateKnowLocations.UpdateMovement(direction);
        }
    }
}
