﻿using GameDataStorage;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MapMovement
{
    /// <summary>
    /// This static class exists to check if we have visited every room.
    /// </summary>
    public static class CheckKnownLocations
    {
        /// <summary>
        /// Check if we have visited every room.
        /// </summary>
        /// <returns>Whether we have visited every room.</returns>
        public static bool HasVisitedAllLocations()
        {
            for (int row = 0; row < GameData.GetMap.GetRooms.GetLength(0); row++)
            {
                for (int col = 0; col < GameData.GetMap.GetRooms.GetLength(1); col++)
                {
                    // If the room exists and we haven't visited it, we haven't visited all rooms
                    if ((GameData.GetMap.GetRooms[row, col] != null) && (GameData.GetKnownLocations.GetRoomsVisited[row, col] == false))
                        return false;
                }
            }

            return true; // If we got here, we have seen all locations
        }
    }
}
