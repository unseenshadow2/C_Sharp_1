﻿using RoomManager;
using RoomManager.Map;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MapMovement
{
    /// <summary>
    /// This class handles moving from a room to another connected
    /// room.
    /// </summary>
    public static class RoomMove
    {
        /// <summary>
        /// Check if you can move to another room and return the room moved to.
        /// Returns null if you can't move.
        /// </summary>
        /// <param name="direction">The direction to travel.</param>
        /// <param name="room">The room that is being traveled from.</param>
        /// <returns>The room to be moved to or null.</returns>
        public static Room MoveRoom(Direction direction, Room room)
        {
            switch (direction)
            {
                case Direction.North:
                    return room.GetNorth;
                case Direction.South:
                    return room.GetSouth;
                case Direction.East:
                    return room.GetEast;
                case Direction.West:
                    return room.GetWest;
                default:
                    return null;
            }
        }
    }
}
