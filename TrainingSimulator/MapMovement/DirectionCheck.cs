﻿using GameDataStorage;
using RoomManager;
using RoomManager.Map;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MapMovement
{
    /// <summary>
    /// This static class handles checking if you can move in a direction.
    /// </summary>
    public static class DirectionCheck
    {
        /// <summary>
        /// Check whether we can move a direction.
        /// </summary>
        /// <param name="direction">The direction we are traveling in.</param>
        /// <param name="room">The room we are moving from.</param>
        /// <returns>Returns whether we moved.</returns>
        public static bool CheckDirection(Direction direction, Room room)
        {
            switch (direction)
            {
                case Direction.North:
                    return (room.GetNorth != null);
                case Direction.South:
                    return (room.GetSouth != null);
                case Direction.East:
                    return (room.GetEast != null);
                case Direction.West:
                    return (room.GetWest != null);
                default:
                    return false;
            }
        }

        /// <summary>
        /// Check whether we can move a direction.
        /// </summary>
        /// <param name="direction">The direction we are traveling in.</param>
        /// <returns>Returns whether we moved.</returns>
        public static bool CheckDirection(Direction direction)
        {
            return CheckDirection(direction, GameData.GetRoom);
        }
    }
}
