﻿using GameDataStorage;
using RoomManager.Map;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MapMovement
{
    /// <summary>
    /// This class is to handle updating the KnownLocations
    /// </summary>
    public static class UpdateKnowLocations
    {
        /// <summary>
        /// Update the known locations after we have moved. Assumes you have already checked that you can
        /// move in the given direction.
        /// </summary>
        /// <param name="direction">The direction that we moved</param>
        public static void UpdateMovement(Direction direction)
        {
            UpdateLocation(direction);

            GameData.GetKnownLocations.GetRoomsVisited[GameData.GetKnownLocations.GetRow, GameData.GetKnownLocations.GetCol] = true;
        }

        /// <summary>
        /// Update our location variables within our known locations
        /// </summary>
        /// <param name="direction">The direction that we moved</param>
        private static void UpdateLocation(Direction direction)
        {
            switch (direction)
            {
                case Direction.North:
                    GameData.GetKnownLocations.SetRow = GameData.GetKnownLocations.GetRow - 1;
                    break;
                case Direction.South:
                    GameData.GetKnownLocations.SetRow = GameData.GetKnownLocations.GetRow + 1;
                    break;
                case Direction.East:
                    GameData.GetKnownLocations.SetCol = GameData.GetKnownLocations.GetCol + 1;
                    break;
                case Direction.West:
                    GameData.GetKnownLocations.SetCol = GameData.GetKnownLocations.GetCol - 1;
                    break;
            }
        }
    }
}
