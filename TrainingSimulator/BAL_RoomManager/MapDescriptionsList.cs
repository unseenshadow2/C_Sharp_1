﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RoomManager.Map.Descriptions
{
    public static class MapDescriptionsList
    {
        public const string descriptionList = 
@"This document will be parsed to provide the data for the rooms' names and descriptions.
Due to how regular expressions work, any text that isn't in the right format is ignored.

{0, 1}, {Entry}, {This room has a door to the south and the stairs to the north. The room is otherwise barren.}

{1, 0}, {Drone Bay}, {This room is filled with drones waiting to be maintained.}
{1, 1}, {Manufactury}, {This room is filled with complex machinery for producing any number of goods.}
{1, 2}, {Warehouse}, {This room contains shelves of countless different items, ranging from foodstuffs to drone spare parts.}

{2, 0}, {Offices}, {This room is a densely packed room of cubicles, office chairs, and computers, this room appears to house computer workers.}
{2, 1}, {Test Room}, {This room contains a large volume of testing equipment for some unknown purpose.}
{2, 2}, {Fancy Office}, {This room is likely the fanciest office one could have. Liquor shelves, a chair worth no less than $10,000, and a desk of real wood.}

{3, 1}, {Store Front}, {This room is actually on the surface, coming out into another building. With glass windows to the outside world and countless shelving units, this is the perfect place to leave from.}";
    }
}
