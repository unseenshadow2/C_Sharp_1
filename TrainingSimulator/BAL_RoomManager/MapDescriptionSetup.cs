﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RoomManager.Map.Descriptions
{
    /// <summary>
    /// This class exists soley for assigning room names and descriptions within the map.
    /// </summary>
    public static class MapDescriptionSetup
    {
        /// <summary>
        /// Assign the names and descriptions for a map at once.
        /// </summary>
        /// <param name="names">A three dimensional array of strings. The array is structure as
        /// [row, column, data], where the name is in position 0 of data and the description is in position 1.</param>
        /// <param name="map">The map whose rooms are to be assigned.</param>
        public static void AssignNamesAndDescriptions(String[,,] names, Map map)
        {
            for (int row = 0; row < names.GetLength(0); row++)
            {
                for (int col = 0; col < names.GetLength(1); col++)
                {
                    // When the room is valid, assign the name and description.
                    if (map.GetRooms[row, col] != null)
                    {
                        map.GetRooms[row, col].SetName = names[row, col, 0];
                        map.GetRooms[row, col].SetDescription = names[row, col, 1];
                    }
                }
            }
        }
    }
}
