﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MapMovement;
using RoomManager.Map;

namespace PAL
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            UpdateControls(); // Make sure we properly enable and disable controls
        }

        private void MoveButton(object sender, EventArgs e)
        {
            Button button = sender as Button; // Make sure we can access the name of the sender

            // Move in the given direction
            if (button.Name == "btnNorth") MapMove.Move(Direction.North);
            else if (button.Name == "btnSouth") MapMove.Move(Direction.South);
            else if (button.Name == "btnEast") MapMove.Move(Direction.East);
            else if (button.Name == "btnWest") MapMove.Move(Direction.West);
            else return; // Not an expected input, do nothing

            UpdateControls(); // Update the controls
            DisplaySuccess(); // Show success if we have achieved it
        }

        private void UpdateControls()
        {
            // Get where we can move
            AvailableDirections directions = AvailableDirections.GetAvailableDirections();

            // Show where we can move
            btnNorth.Enabled = directions.GetNorth;
            btnSouth.Enabled = directions.GetSouth;
            btnEast.Enabled = directions.GetEast;
            btnWest.Enabled = directions.GetWest;

            // Update our string display
            lblNameDescription.Text = GetRoomString.GetString();
        }

        private void DisplaySuccess()
        {
            // If we have visited all rooms, alert the agent
            if (CheckKnownLocations.HasVisitedAllLocations())
            {
                MessageBox.Show("I hope you found something in there, agent, because you have explored every room.", "Congratulations");
            }
        }
    }
}
