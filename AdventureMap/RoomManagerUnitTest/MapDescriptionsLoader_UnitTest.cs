﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using RoomManager.Map.Descriptions;
using System.Diagnostics;

namespace RoomManagerUnitTest
{
    [TestClass]
    public class MapDescriptionsLoader_UnitTest
    {
        [TestMethod]
        public void MapDescriptionsLoader_FileLoadTest()
        {
            MapDescriptionsLoader mdl = new MapDescriptionsLoader();

            mdl.LoadNameFromFile(@"F:\CSharp1\AdventureMap\RoomDescriptions.txt");

            Assert.IsNotNull(mdl.GetNames[0, 1, 0]);
        }
    }
}
