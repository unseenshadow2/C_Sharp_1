﻿using System;

namespace RoomManager
{
    /// <summary>
    /// This is a data storage class for an individual room.
    /// </summary>
    public class Room
    {
        // Connected rooms - If a direction is null, that means that we don't have a door on the wall in that direction
        private Room north = null;
        private Room south = null;
        private Room east = null;
        private Room west = null;

        // The name of the room
        private String name = null;

        // The description of the room, to be displayed when the room is entered
        private String description = null;

        /// <summary>
        /// Default constructor. This instantiates an empty Room.
        /// </summary>
        public Room() { }

        /// <summary>
        /// Constructor. This instantiates a Room with only a name and description set.
        /// </summary>
        /// <param name="_name">The name of the Room</param>
        /// <param name="_description">The description of the Room</param>
        public Room(String _name, String _description)
        {
            this.name = _name;
            this.description = _description;
        }

        /// <summary>
        /// Constructor. This instantiates a Room with all values set.
        /// </summary>
        /// <param name="_name">The name of the Room</param>
        /// <param name="_description">The description of the Room</param>
        /// <param name="_north">The Room to the north. No room is connected when null. </param>
        /// <param name="_south">The Room to the south. No room is connected when null. </param>
        /// <param name="_east">The Room to the east. No room is connected when null. </param>
        /// <param name="_west">The Room to the west. No room is connected when null. </param>
        public Room(String _name, String _description, Room _north, Room _south, Room _east, Room _west)
            : this(_name, _description)
        {
            SetAttachedRooms(_north, _south, _east, _west);
        }

        /// <summary>
        /// Assigns the attached rooms.
        /// </summary>
        /// <param name="_north">The Room to the north</param>
        /// <param name="_south">The Room to the south</param>
        /// <param name="_east">The Room to the east</param>
        /// <param name="_west">The Room to the west</param>
        public void SetAttachedRooms(Room _north, Room _south, Room _east, Room _west)
        {
            this.north = _north;
            this.south = _south;
            this.east = _east;
            this.west = _west;
        }

        // Getters and Setters
        public Room GetNorth { get { return north; } }
        public Room SetNorth { set { north = value; } }

        public Room GetSouth { get { return south; } }
        public Room SetSouth { set { south = value; } }

        public Room GetEast { get { return east; } }
        public Room SetEast { set { east = value; } }

        public Room GetWest { get { return west; } }
        public Room SetWest { set { west = value; } }

        public String GetName { get { return name; } }
        public String SetName { set { name = value; } }

        public String GetDescription { get { return description; } }
        public String SetDescription { set { description = value; } }
    }
}
