﻿using GameDataStorage;
using RoomManager.Map;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MapMovement
{
    /// <summary>
    /// This static class handles checking if you can move in a direction.
    /// </summary>
    public static class MapDirectionCheck
    {
        /// <summary>
        /// Check whether we can move a direction.
        /// </summary>
        /// <param name="direction">The direction we are travelling in.</param>
        /// <returns>Returns whether we moved.</returns>
        public static bool CheckDirection(Direction direction)
        {
            switch (direction)
            {
                case Direction.North:
                    return (GameData.GetRoom.GetNorth != null);
                case Direction.South:
                    return (GameData.GetRoom.GetSouth != null);
                case Direction.East:
                    return (GameData.GetRoom.GetEast != null);
                case Direction.West:
                    return (GameData.GetRoom.GetWest != null);
                default:
                    return false;
            }
        }
    }
}
