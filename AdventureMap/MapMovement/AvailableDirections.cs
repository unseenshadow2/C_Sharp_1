﻿using GameDataStorage;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MapMovement
{
    /// <summary>
    /// This class handles communication in what directions we can move.
    /// </summary>
    public class AvailableDirections
    {
        // The directions we can move
        bool north;
        bool south;
        bool east;
        bool west;

        /// <summary>
        /// Get the currently available directions.
        /// </summary>
        /// <returns>Returns an AvailableDirections containing the currently available directions.</returns>
        public static AvailableDirections GetAvailableDirections()
        {
            AvailableDirections toReturn = new AvailableDirections();

            toReturn.north = (GameData.GetRoom.GetNorth != null);
            toReturn.south = (GameData.GetRoom.GetSouth != null);
            toReturn.east = (GameData.GetRoom.GetEast != null);
            toReturn.west = (GameData.GetRoom.GetWest != null);

            return toReturn;
        }

        // Getters and Setters
        public bool GetNorth { get { return north; } }

        public bool GetSouth { get { return south; } }

        public bool GetEast { get { return east; } }

        public bool GetWest { get { return west; } }

    }
}
