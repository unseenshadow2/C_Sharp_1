﻿using RoomManager;
using RoomManager.Map;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameDataStorage
{
    /// <summary>
    /// This is a static class. GameData handles storing all of the active game data,
    /// allowing the buisness access libraries can access the data across libraries.
    /// </summary>
    public static class GameData
    {
        // Our active map
        private static Map map = new Map();

        // Where we have visited
        private static KnownLocations knownLocations = new KnownLocations();

        // Our current room
        private static Room room = map.GetRooms[0, 1];

        // Getters and Setters
        public static Map GetMap { get { return map; } }

        public static KnownLocations GetKnownLocations { get { return knownLocations; } }

        public static Room GetRoom { get { return room; } }
        public static Room SetRoom { set { room = value; } }
    }
}
