﻿using GameDataStorage;
using RoomManager.Map;
using RoomManager.Map.Descriptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MapStartup
{
    public static class StartUp
    {
        private static MapDescriptionsLoader loader = new MapDescriptionsLoader();

        /// <summary>
        /// Handle setting up the map in a single line. Particularly the names and descriptions.
        /// Uses the default map from GameData.
        /// </summary>
        public static void SetupMap()
        {
            SetupMap(GameData.GetMap);
        }

        /// <summary>
        /// Handle setting up the map in a single line. Particularly the names and descriptions.
        /// </summary>
        /// <param name="map">The map that we need to setup.</param>
        public static void SetupMap(Map map)
        {
            loader.LoadNameFromFile("RoomDescriptions.txt");
            MapDescriptionSetup.AssignNamesAndDescriptions(loader.GetNames, map);
        }
    }
}
