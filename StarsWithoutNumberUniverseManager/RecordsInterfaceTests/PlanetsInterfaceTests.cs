﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using RecordsInterface;
using RecordsSQL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RecordsInterface.Tests
{
    [TestClass()]
    public class PlanetsInterfaceTests
    {
        [TestMethod()]
        public void HasValidLocationTest_pass()
        {
            planet toTest = new planet();

            toTest.location = "0003";

            Assert.IsTrue(PlanetsInterface.HasValidLocation(toTest));
        }

        [TestMethod()]
        public void HasValidLocationTest_failShort()
        {
            planet toTest = new planet();

            toTest.location = "003";

            Assert.IsFalse(PlanetsInterface.HasValidLocation(toTest));
        }

        [TestMethod()]
        public void HasValidLocationTest_failNull()
        {
            planet toTest = new planet();

            toTest.location = null;

            Assert.IsFalse(PlanetsInterface.HasValidLocation(toTest));
        }

        [TestMethod()]
        public void HasValidLocationTest_failBadCharacter()
        {
            planet toTest = new planet();

            toTest.location = "00A3";

            Assert.IsFalse(PlanetsInterface.HasValidLocation(toTest));
        }

        // The following test functions test GetPlanetsByLocation()
        // exception throwing
        [TestMethod()]
        public void GetPlanetsByLocationTest_CatchInputTooShort()
        {
            Assert.ThrowsException<InvalidPlanetException>(() => PlanetsInterface.GetPlanetsByLocation("000"));
        }

        [TestMethod()]
        public void GetPlanetsByLocationTest_CatchNullInput()
        {
            Assert.ThrowsException<InvalidPlanetException>(() => PlanetsInterface.GetPlanetsByLocation(null));
        }

        [TestMethod()]
        public void GetPlanetsByLocationTest_CatchBadCharacter()
        {
            Assert.ThrowsException<InvalidPlanetException>(() => PlanetsInterface.GetPlanetsByLocation("000A"));
        }

        // NOTE: This test requires at least one planet in the database
        //       with a location of 0003
        [TestMethod()]
        public void GetPlanetsByLocationTest_Pass()
        {
            try
            {
                planet[] results = PlanetsInterface.GetPlanetsByLocation("0003");
                Assert.IsNotNull(results);
            }
            catch (InvalidPlanetException ex)
            {
                Assert.Fail();
            }
        }
    }
}