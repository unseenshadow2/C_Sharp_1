﻿using StarsRecords;
using StarsRecords.Main_Sheets;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RecordsManagers
{
    public class Sector
    {
        public NPCManager npcs = new NPCManager();
        public FactionAssetManager factionAssets = new FactionAssetManager();
        public List<Planet> planets = new List<Planet>();
        public List<Faction> factions = new List<Faction>();
        public List<Alien> aliens = new List<Alien>();
    }
}
