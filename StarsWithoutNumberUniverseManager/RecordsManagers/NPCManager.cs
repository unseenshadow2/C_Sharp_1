﻿using StarsRecords.Supporting_Sheets;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RecordsManagers
{
    public class NPCManager
    {
        public List<NPC> npcs = new List<NPC>();

        /// <summary>
        /// Returns the NPC with the given ID or returns null.
        /// </summary>
        /// <param name="id">The ID of the NPC being searched for</param>
        /// <returns>The NPC with that ID or null if no NPC exists</returns>
        public NPC GetNPCById(uint id)
        {
            foreach (NPC x in npcs)
            {
                if (x.id == id) return x;
            }

            return null;
        }

        /// <summary>
        /// Registers an NPC and gives it a unique ID.
        /// </summary>
        /// <param name="toRegister">The npc to register</param>
        public void RegisterNPC(NPC toRegister)
        {
            uint highestId = GetNewId();
            
            toRegister.id = highestId;

            npcs.Add(toRegister);
        }

        /// <summary>
        /// Registers multiple NPCs and gives them each a unique ID.
        /// </summary>
        /// <param name="toRegister">The npcs to register</param>
        public void RegisterNPCs(NPC[] toRegister)
        {
            uint id = GetNewId();

            foreach (NPC x in toRegister)
            {
                x.id = id;
                ++id;
            }

            npcs.AddRange(toRegister);
        }
        
        /// <summary>
        /// Gets a new id.
        /// </summary>
        /// <returns>The new id</returns>
        private uint GetNewId()
        {
            uint highestId = 0;

            foreach (NPC x in npcs)
            {
                if (x.id > highestId) highestId = x.id;
            }

            return ++highestId;
        }
    }
}
