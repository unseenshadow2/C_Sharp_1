﻿using StarsRecords.Supporting_Sheets;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RecordsManagers
{
    public class FactionAssetManager
    {
        public List<FactionAsset> assets = new List<FactionAsset>();

        /// <summary>
        /// Returns the FactionAsset with the given ID or returns null.
        /// </summary>
        /// <param name="id">The ID of the FactionAsset being searched for</param>
        /// <returns>The FactionAsset with that ID or null if no FactionAsset exists</returns>
        public FactionAsset GetFactionAssetById(uint id)
        {
            foreach (FactionAsset x in assets)
            {
                if (x.id == id) return x;
            }

            return null;
        }

        /// <summary>
        /// Registers an FactionAsset and gives it a unique ID.
        /// </summary>
        /// <param name="toRegister">The FactionAsset to register</param>
        public void RegisterFactionAsset(FactionAsset toRegister)
        {
            uint highestId = GetNewId();

            toRegister.id = highestId;

            assets.Add(toRegister);
        }

        /// <summary>
        /// Registers multiple FactionAssets and gives them each a unique ID.
        /// </summary>
        /// <param name="toRegister">The FactionAssets to register</param>
        public void RegisterAssets(FactionAsset[] toRegister)
        {
            uint id = GetNewId();

            foreach (FactionAsset x in toRegister)
            {
                x.id = id;
                ++id;
            }

            assets.AddRange(toRegister);
        }

        /// <summary>
        /// Gets a new id.
        /// </summary>
        /// <returns>The new id</returns>
        private uint GetNewId()
        {
            uint highestId = 0;

            foreach (FactionAsset x in assets)
            {
                if (x.id > highestId) highestId = x.id;
            }

            return ++highestId;
        }
    }
}
