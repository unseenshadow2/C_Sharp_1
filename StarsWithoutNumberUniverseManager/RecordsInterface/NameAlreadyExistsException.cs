﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RecordsInterface
{
    /// <summary>
    /// This is an exception to display that a name
    /// given to a planet already exists.
    /// </summary>
    public class NameAlreadyExistsException : Exception
    {
        public NameAlreadyExistsException(string message)
            : base(message)
        {

        }
    }
}
