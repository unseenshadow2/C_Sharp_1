﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RecordsInterface
{
    /// <summary>
    /// This is an exception to represent that a planet given to a function
    /// is invalid.
    /// </summary>
    public class InvalidPlanetException : Exception
    {
        public InvalidPlanetException(string message)
            : base(message)
        {

        }
    }
}
