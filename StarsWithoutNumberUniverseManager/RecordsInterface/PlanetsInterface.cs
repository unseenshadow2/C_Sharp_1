﻿using RecordsSQL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RecordsInterface
{
    /// <summary>
    /// A class to act as the in-between for the database and the front end.
    /// NOTE:   This class breaks standard to offer several public functions
    ///         as they are all just LINQ wrappers.
    ///         
    ///         Also, several functions that handle simple checks are exposed.
    ///         This is so that the GUI can check if there is an issue with the
    ///         actions it is about to take before causing exceptions.
    /// </summary>
    public static class PlanetsInterface
    {
        /// <summary>
        /// Check if a planet is valid to send to the database. Does not check id.
        /// </summary>
        /// <param name="toCheck">The planet to be checked.</param>
        /// <returns>Whether the given planet is valid.</returns>
        public static bool IsPlanetValid(planet toCheck)
        {
            if (HasValidLocation(toCheck) == false) return false;
            else if (toCheck.name == null) return false;
            else if (toCheck.name.Length <= 0) return false;
            else if (toCheck.name == "") return false;
            else return true;
        }

        /// <summary>
        /// Checks whether the location value of a planet is valid or not.
        /// </summary>
        /// <param name="toCheck">The planet to check.</param>
        /// <returns>Whether the location value is valid.</returns>
        public static bool HasValidLocation(planet toCheck)
        {
            if (toCheck.location == null) return false;
            else if (toCheck.location.Length != 4) return false;

            // Make sure that all four characters are valid
            for (int i = 0; i < 4; i++)
            {
                if (!((toCheck.location[i] >= '0') && (toCheck.location[i] <= '9'))) return false;
            }

            return true;
        }

        /// <summary>
        /// Check if the planet given has a valid name.
        /// </summary>
        /// <param name="toCheck">The planet to check</param>
        /// <returns>Whether the planet has a valid name.</returns>
        public static bool HasValidName(planet toCheck)
        {
            return HasValidNames(new planet[] { toCheck });
        }

        /// <summary>
        /// Check if all the planets given have valid names.
        /// </summary>
        /// <param name="toCheck">The planets to check</param>
        /// <returns>Whether the planets have valid names.</returns>
        public static bool HasValidNames(planet[] toCheck)
        {
            bool toReturn = false;

            using (stars_sectorEntities db = new stars_sectorEntities())
                    toReturn = HasValidNames(toCheck, db);

            return toReturn;
        }

        /// <summary>
        /// Check if the planet given has a valid name.
        /// </summary>
        /// <param name="toCheck">The planet to check</param>
        /// <param name="db">The database in which the planets are kept.</param>
        /// <returns>Whether the planet has a valid name.</returns>
        public static bool HasValidName(planet toCheck, stars_sectorEntities db)
        {
            return HasValidNames(new planet[] { toCheck }, db);
        }

        /// <summary>
        /// Check if all the planets given have valid names.
        /// </summary>
        /// <param name="toCheck">The planets to check</param>
        /// <param name="db">The database in which the planets are kept.</param>
        /// <returns>Whether the planets have valid names.</returns>
        public static bool HasValidNames(planet[] toCheck, stars_sectorEntities db)
        {
            foreach (planet p in toCheck)
            {
                planet query = (from planet in db.planets
                                where (planet.name == p.name)
                                select planet).FirstOrDefault();

                // If query isn't null, then we have a matching planet and the name is bad.
                if (query != null) return false;
            }

            return true;
        }

        /// <summary>
        /// Checks whether a planet that is "new" would have a name conflict
        /// and if it has a valid location.
        /// </summary>
        /// <param name="newPlanet">The planet to be checked.</param>
        /// <returns>Whether the planet is a valid "new" planet or not.</returns>
        public static bool IsNewPlanetValid(planet newPlanet)
        {
            bool toReturn = true;

            if (IsPlanetValid(newPlanet) == false) toReturn = false;
            if (HasValidName(newPlanet) == false) toReturn = false;

            return toReturn;
        }

        /// <summary>
        /// Checks whether a planet that is "new" would have a name conflict
        /// and if it has a valid location.
        /// </summary>
        /// <param name="newPlanet">The planet to be checked.</param>
        /// <param name="db">The database where the planets are kept.</param>
        /// <returns>Whether the planet is a valid "new" planet or not.</returns>
        public static bool IsNewPlanetValid(planet newPlanet, stars_sectorEntities db)
        {
            bool toReturn = true;

            if (IsPlanetValid(newPlanet) == false) toReturn = false;
            if (HasValidName(newPlanet, db) == false) toReturn = false;

            return toReturn;
        }

        /// <summary>
        /// Get all planets currently in the database.
        /// </summary>
        /// <returns>An array of planets ordered by id</returns>
        public static planet[] GetAllPlanets()
        {
            planet[] query;

            // Use LINQ to get all of the planets by id
            using (stars_sectorEntities db = new stars_sectorEntities())
            {
                query = (from planet in db.planets
                                 orderby planet.id
                                 select planet).ToArray();
            }

            return query;
        }

        /// <summary>
        /// Gets all planets that contain the given name in their name.
        /// </summary>
        /// <param name="name">The string that contains either a portion or the
        ///                    whole name of the planet that is being searched for.</param>
        /// <returns>A list of planets that contain the name.</returns>
        public static planet[] GetPlanetsByName(string name)
        {
            planet[] query;

            // Use LINQ to get the planets by name
            using (stars_sectorEntities db = new stars_sectorEntities())
            {
                query = (from planet in db.planets
                         orderby planet.name
                         where planet.name.Contains(name)
                         select planet).ToArray();
            }

            return query;
        }

        /// <summary>
        /// Get all planets at the given location.
        /// </summary>
        /// <param name="loc">A 4 character string containing the location of the planet.</param>
        /// <returns>An array of planets with the matching location.</returns>
        public static planet[] GetPlanetsByLocation(string loc)
        {
            // Through a invalid planet exception stating that the location is invalid
            if (HasValidLocation(new planet { location = loc }) == false) throw new InvalidPlanetException("Location given to GetPlanetsByLocation is invalid.");
            
            planet[] query;

            // Use LINQ to get the planets by location
            using (stars_sectorEntities db = new stars_sectorEntities())
            {
                query = (from planet in db.planets
                         orderby planet.name
                         where (planet.location == loc)
                         select planet).ToArray();
            }

            return query;
        }

        /// <summary>
        /// Get an array of planets if their id matches the given id. NOTE:
        /// Only 1 or less planets should ever be returned.
        /// </summary>
        /// <param name="id">The id of the planet sought after.</param>
        /// <returns>An array of planets. Should contain one planet at most.</returns>
        public static planet[] GetPlanetsById(int id)
        {
            planet[] query;

            // Use LINQ to get the planets by location
            using (stars_sectorEntities db = new stars_sectorEntities())
            {
                query = (from planet in db.planets
                         orderby planet.name
                         where (planet.id == id)
                         select planet).ToArray();
            }

            return query;
        }

        /// <summary>
        /// Add a new planet to the database.
        /// </summary>
        /// <param name="toAdd">The planet to add. Planets of id 0 will be given an id.</param>
        public static void AddPlanet(planet toAdd)
        {
            // Use the ranged version, converting our value to a single
            // item array.
            AddPlanets(new planet[] { toAdd });
        }

        /// <summary>
        /// Add an array of planets to the database.
        /// </summary>
        /// <param name="toAdd">The array of planets to add. Planets of id 0 will be given an id.</param>
        public static void AddPlanets(planet[] toAdd)
        {
            // Add the range to the database and update
            using (stars_sectorEntities db = new stars_sectorEntities())
            {
                // Catch duplicate names and check validity
                foreach (planet p in toAdd)
                {
                    if (!IsNewPlanetValid(p, db)) throw new InvalidPlanetException("Invalid planet sent to AddPlanets(). New planets must have unique names;");
                }

                db.planets.AddRange(toAdd);
                db.SaveChanges();

                // Update ids after save
                foreach (planet p in toAdd)
                {
                    planet tempToCopy = (from planet in db.planets
                                         orderby planet.name
                                         where (planet.name == p.name)
                                         select planet).First();

                    p.DeepCopy(tempToCopy);
                }
            }
        }

        /// <summary>
        /// Updates a single planet on the database.
        /// </summary>
        /// <param name="toUpdate">The updated planet. Planets of id 0 will be given an id.</param>
        public static void UpdatePlanet(planet toUpdate)
        {
            UpdatePlanets(new planet[] { toUpdate });
        }

        /// <summary>
        /// Updates a multiple planets on the database.
        /// </summary>
        /// <param name="toUpdate">The updated planets. Planets of id 0 will be given an id.</param>
        public static void UpdatePlanets(planet[] toUpdate)
        {
            // Error checking...
            foreach (planet p in toUpdate)
            {
                if (!IsPlanetValid(p)) throw new InvalidPlanetException("A planet sent to UpdatePlanets() is invalid");
            }

            // Update multiple planets
            using (stars_sectorEntities db = new stars_sectorEntities())
            {
                // Update each of the planets
                foreach (planet p in toUpdate)
                {
                    // When our id is 0, it is a new planet.
                    if (p.id == 0)
                    {
                        if (HasValidName(p, db) == false) throw new InvalidPlanetException("A new planet with a non-unique name sent to UpdatePlanets()");
                        db.planets.Add(p);
                    }
                    else // Update existing planets
                    {
                        // Find the entry, then copy the values
                        planet dbplanet = (from planet in db.planets
                                           where (planet.id == p.id)
                                           select planet).First();
                        
                        dbplanet.DeepCopy(p);
                    }
                }

                // Save changes when complete
                db.SaveChanges();

                // Update ids after a save
                foreach (planet p in toUpdate)
                {
                    planet tempToCopy = (from planet in db.planets
                                         orderby planet.name
                                         where (planet.name == p.name)
                                         select planet).First();

                    p.DeepCopy(tempToCopy);
                }
            }
        }

        /// <summary>
        /// Delete a planet with the id given from the database.
        /// </summary>
        /// <param name="id">The id of the planet to be deleted.</param>
        public static void DeletePlanet(int id)
        {
            DeletePlanets(new int[] { id });
        }

        /// <summary>
        /// Delete planets with the ids given from the database.
        /// </summary>
        /// <param name="ids">The ids of the planets to be deleted.</param>
        public static void DeletePlanets(int[] ids)
        {
            using (stars_sectorEntities db = new stars_sectorEntities())
            {
                // Update each of the planets
                foreach (int id in ids)
                {
                    planet dbplanet = (from planet in db.planets
                                       where (planet.id == id)
                                       select planet).First();

                    // If the planet exists, delete it.
                    if (dbplanet != null) db.planets.Remove(dbplanet);
                }

                // Save changes when complete
                db.SaveChanges();
            }
        }
    }
}
