﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RecordsSQL
{
    /// <summary>
    /// This is an extension of the planet class to add a deep copy function.
    /// </summary>
    public partial class planet
    {
        /// <summary>
        /// Copies all values from the from planet to this.
        /// </summary>
        /// <param name="from">The planet whose values are being copied.</param>
        public void DeepCopy(planet from)
        {
            id = from.id;
            location = string.Copy(from.location);
            name = string.Copy(from.name);
            enemies = string.Copy(from.enemies);
            friends = string.Copy(from.friends);
            complications = string.Copy(from.complications);
            places = string.Copy(from.places);
            things = string.Copy(from.things);
            capital_and_government = string.Copy(from.capital_and_government);
            cultural_notes = string.Copy(from.cultural_notes);
            party_activities = string.Copy(from.party_activities);
            notes = string.Copy(from.notes);
            tags = string.Copy(from.tags);
            tech_level = string.Copy(from.tech_level);
            atmosphere = string.Copy(from.atmosphere);
            temperature = string.Copy(from.temperature);
            biosphere = string.Copy(from.biosphere);
        }
    }
}
