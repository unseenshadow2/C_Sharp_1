﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StarsRecords.Supporting_Sheets
{
    public class FactionAsset
    {
        public string name;
        public string type;
        public string attack;
        public string counter;
        public string location;
        public string notes;
        public int maxHealth;
        public int currentHealth;
        public uint id;
    }
}
