﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StarsRecords.Supporting_Sheets
{
    public class NPC
    {
        public string name;
        public string health;
        public string damage;
        public string description;
        public string powers; // This should normally be left blank
        public string notes;
        public string toHit;
        public string AC;
        public int skillBonus;
        public int saves;
        public int move;
        public int moral;
        public uint id;
    }
}
