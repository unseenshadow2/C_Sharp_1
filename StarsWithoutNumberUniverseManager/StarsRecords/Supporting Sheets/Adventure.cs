﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StarsRecords.Main_Sheets
{
    public class Adventure
    {
        public string name;
        public string location;
        public string expectedLevels;
        public string dateRun;
        public string participants;
        public string adventureOutline;
        public string rewardsAvailable;
        public string summaryOfEvents;
        public string notes;
        public List<uint> npcs = new List<uint>();
    }
}
