﻿using StarsRecords.Main_Sheets;
using StarsRecords.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StarsRecords
{
    public class Planet
    {
        public string name;
        public string enemies;
        public string friends;
        public string complications;
        public string places;
        public string things;
        public string capitalAndGovernment;
        public string culturalNotes;
        public string partyActivities;
        public string notes;
        public string[] tags = new string[2];
        public List<Adventure> adventures = new List<Adventure>();
        public List<uint> npcs = new List<uint>();
        public String population;
        public TechLevel techLevel;
        public PlanetAtmosphere atmosphere;
        public PlanetTemperature tempurature;
        public PlanetBiosphere biosphere;
    }
}
