﻿using StarsRecords.Supporting_Sheets;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StarsRecords.Main_Sheets
{
    public enum BodyType { Humanlike, Avian, Reptilian, Insectile, Exotic, Hybrid };

    public class Alien
    {
        public string name;
        public string plural;
        public string homeworld;
        public string[] lenses = new string[2];
        public string appearance;
        public string government;
        public string goals;
        public string adventureHooks;
        public string notes;
        public BodyType bodyType;
        public List<uint> enemies = new List<uint>();
    }
}
