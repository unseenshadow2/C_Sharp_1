﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StarsRecords
{
    public class Faction
    {
        public string name;
        public string income;
        public string homeworld;
        public string[] factionTags = new string[2];
        public string goalProgress;
        public string historyAndPurpose;
        public string partyInvolvement;
        public string notes;
        public int currentFacCreds;
        public int forceScore;
        public int cunningScore;
        public int wealthScore;
        public int currentHp;
        public int maxHp;
        public List<uint> npcs = new List<uint>();
        public List<uint> assets = new List<uint>();
    }
}
