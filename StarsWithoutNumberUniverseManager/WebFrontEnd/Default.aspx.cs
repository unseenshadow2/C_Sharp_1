﻿using RecordsInterface;
using RecordsSQL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebFrontEnd
{
    public partial class _Default : Page
    {
        // The id of the current planet
        // Stores and retrieves the value from the session
        private int currentId
        {
            get
            {
                int? stateId = Session["currentId"] as int?;
                return (stateId == null) ? 0 : (int)stateId;
            }
            set
            {
                Session["currentId"] = value;
            }
        }

        /// <summary>
        /// Load the page and set the title.
        /// </summary>
        /// <param name="sender">The object that called the function.</param>
        /// <param name="e">The event arguments.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            // Give us access to alert boxes
            MessageBox.SetPage(Page);

            if(!Page.IsPostBack)
            {
                Page.Title = "Stars Without Number Planet Manager";
                updateListbox();
            }  
            else
            {
                
            }
        }

        /// <summary>
        /// Prepare the page for creating a new planet.
        /// </summary>
        /// <param name="sender">The object that called this function.</param>
        /// <param name="e">The event arguments.</param>
        protected void btnNew_Click(object sender, EventArgs e)
        {
            currentId = 0;
            updateListbox();
            ClearTextboxes();
            UpdateTextboxes();
        }

        /// <summary>
        /// Update an existing planet or add a new one.
        /// </summary>
        /// <param name="sender">The object that called this function.</param>
        /// <param name="e">The event arguments.</param>
        protected void btnSave_Click(object sender, EventArgs e)
        {
            planet toSave = ToPlanet();

            if (PlanetsInterface.IsPlanetValid(toSave))
            {
                // If we aren't a new planet or we are a valid new planet, update
                if ((currentId != 0) || PlanetsInterface.IsNewPlanetValid(toSave))
                {
                    PlanetsInterface.UpdatePlanet(toSave);
                    currentId = toSave.id;
                    updateListbox();
                    UpdateTextboxes();
                }
                else MessageBox.Show("New planet is not valid. Make sure it has a unique name.");
            }
            else MessageBox.Show("Planet is not valid. Make sure it has a location and a name.");
        }

        /// <summary>
        /// Remove the selected planet from existence.
        /// </summary>
        /// <param name="sender">The object that called this function.</param>
        /// <param name="e">The event arguments.</param>
        protected void btnDelete_Click(object sender, EventArgs e)
        {
            int parseOut;
            if (int.TryParse(lbxPlanets.SelectedValue, out parseOut)) // Can't delete what don't exist.
            {
                PlanetsInterface.DeletePlanet(parseOut);
                currentId = 0; // Clear current id
                updateListbox();
                UpdateTextboxes();
            }
            else MessageBox.Show("No planet selected for delete.");
        }

        /// <summary>
        /// Pull down the values from the database and store the names and ids in
        /// the listbox.
        /// </summary>
        private void updateListbox()
        {
            // Bind the listbox
            lbxPlanets.DataSource = PlanetsInterface.GetAllPlanets();
            lbxPlanets.DataBind();

            // Update the selected value of the listbox
            if (currentId == 0) lbxPlanets.ClearSelection();
            else lbxPlanets.SelectedValue = currentId.ToString();
        }

        /// <summary>
        /// Load the selected planet into the web page. 
        /// </summary>
        /// <param name="sender">The object that called the function.</param>
        /// <param name="e">The event arguments.</param>
        protected void btnLoad_Click(object sender, EventArgs e)
        {
            int tempInt;
            if (int.TryParse(lbxPlanets.SelectedValue, out tempInt))
            {
                currentId = tempInt;
                if (currentId == 0) MessageBox.Show("Current Id is zero...");
                else UpdateTextboxes();
            }
            else MessageBox.Show("Selected could not parse");
        }

        /// <summary>
        /// Update the title of the web page and the loaded planet
        /// based off of the currentId.
        /// </summary>
        private void UpdateTextboxes()
        {
            Page.Title = "Stars Without Number Planet Manager - Planet ID: " + currentId;

            // Get our planet by ID
            planet toLoad = PlanetsInterface.GetPlanetsById(currentId).FirstOrDefault();

            // Load it into the web page
            if (toLoad != null)
            {
                LoadFromPlanet(toLoad);
            }
            else
            {
                MessageBox.Show("Could not load planet...");
            }
        }
    }
}