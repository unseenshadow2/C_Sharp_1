﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;

namespace WebFrontEnd
{
    public partial class _Default : Page
    {
        /// <summary>
        /// Clears all the textboxes with the Default page.
        /// </summary>
        private void ClearTextboxes()
        {
            txtLocation.Text = "";
            txtName.Text = "";
            txtFriends.Text = "";
            txtEnemies.Text = "";
            txtComplications.Text = "";
            txtPlaces.Text = "";
            txtThings.Text = "";
            txtCapitalAndGovernment.Text = "";
            txtCulturalNotes.Text = "";
            txtPartyActivities.Text = "";
            txtNotes.Text = "";
            txtTags.Text = "";
            txtTechLevel.Text = "";
            txtAtmosphere.Text = "";
            txtTemperature.Text = "";
            txtBiosphere.Text = "";
        }
    }
}