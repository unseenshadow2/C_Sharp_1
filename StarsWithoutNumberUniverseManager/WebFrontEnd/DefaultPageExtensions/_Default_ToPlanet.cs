﻿using RecordsSQL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;

namespace WebFrontEnd
{
    public partial class _Default : Page
    {
        /// <summary>
        /// Creates a planet from the data within the Default page.
        /// </summary>
        /// <returns>A planet generated from the values in the Default page.</returns>
        private planet ToPlanet()
        {
            planet toReturn = new planet()
            {
                id = currentId,
                location = txtLocation.Text.Trim(),
                name = txtName.Text.Trim(),
                friends = txtFriends.Text.Trim(),
                enemies = txtEnemies.Text.Trim(),
                complications = txtComplications.Text.Trim(),
                places = txtPlaces.Text.Trim(),
                things = txtThings.Text.Trim(),
                capital_and_government = txtCapitalAndGovernment.Text.Trim(),
                cultural_notes = txtCulturalNotes.Text.Trim(),
                party_activities = txtPartyActivities.Text.Trim(),
                notes = txtNotes.Text.Trim(),
                tags = txtTags.Text.Trim(),
                tech_level = txtTechLevel.Text.Trim(),
                atmosphere = txtAtmosphere.Text.Trim(),
                temperature = txtTemperature.Text.Trim(),
                biosphere = txtBiosphere.Text.Trim()
            };

            return toReturn;
        }
    }
}