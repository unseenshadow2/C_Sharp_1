﻿using RecordsSQL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;

namespace WebFrontEnd
{
    public partial class _Default : Page
    {
        /// <summary>
        /// Load the data from a planet into the Default page.
        /// </summary>
        /// <param name="from">The planet to load from.</param>
        private void LoadFromPlanet(planet from)
        {
            currentId = from.id;
            txtLocation.Text = from.location;
            txtName.Text = from.name;
            txtFriends.Text = from.friends;
            txtEnemies.Text = from.enemies;
            txtComplications.Text = from.complications;
            txtPlaces.Text = from.places;
            txtThings.Text = from.things;
            txtCapitalAndGovernment.Text = from.capital_and_government;
            txtCulturalNotes.Text = from.cultural_notes;
            txtPartyActivities.Text = from.party_activities;
            txtNotes.Text = from.notes;
            txtTags.Text = from.tags;
            txtTechLevel.Text = from.tech_level;
            txtAtmosphere.Text = from.atmosphere;
            txtTemperature.Text = from.temperature;
            txtBiosphere.Text = from.biosphere;
        }
    }
}