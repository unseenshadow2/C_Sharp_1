﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;

namespace WebFrontEnd
{
    public static class MessageBox
    {
        private static Page page;

        public static void Show(String Message)
        {
            page.ClientScript.RegisterStartupScript
            (
               page.GetType(),
               "MessageBox",
               "<script language='javascript'>alert('" + Message + "');</script>"
            );
        }

        public static void SetPage(Page newPage)
        {
            page = newPage;
        }
    }
}