﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="WebFrontEnd._Default" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">

    <div class="row">
        <asp:UpdatePanel ID="updPlanets" runat="server">
            <ContentTemplate>
                <div class="col-md-4 col-sm-6 col-xs-12" style="text-align:center">
                    <asp:Label ID="Label1" runat="server" Text="Planets:"></asp:Label><br />
                    <asp:ListBox ID="lbxPlanets" runat="server" style="width: 90%;min-height:500px" 
                        DataTextField="name" DataValueField="id" ></asp:ListBox>
                        <!--OnSelectedIndexChanged="lbxPlanets_SelectedIndexChanged"-->

                    <div class="row">
                        <asp:Button ID="btnNew" class="btn col-sm-6 col-xs-12" runat="server" Text="New" OnClick="btnNew_Click"/>
                        <asp:Button ID="btnLoad" class="btn col-sm-6 col-xs-12" runat="server" Text="Load" OnClick="btnLoad_Click" />
                        <asp:Button ID="btnSave" class="btn col-sm-6 col-xs-12" runat="server" Text="Save" OnClick="btnSave_Click"/>
                        <asp:Button ID="btnDelete" class="btn col-sm-6 col-xs-12" runat="server" Text="Delete" OnClick="btnDelete_Click"/>
                    </div>                    
                </div>
            <%--</ContentTemplate>
        </asp:UpdatePanel>
    
        <asp:UpdatePanel ID="updGeneralInfo" runat="server">
            <ContentTemplate>--%>
                <div class="col-md-4 col-sm-6 col-xs-12 text-xs-center text-md-left">
                    <asp:Label ID="Label2" runat="server" Text="Location:"></asp:Label><br />
                    <asp:TextBox ID="txtLocation" runat="server" style="width: 50px"></asp:TextBox><br /><br />

                    <asp:Label ID="Label3" runat="server" Text="Name:"></asp:Label><br />
                    <asp:TextBox ID="txtName" runat="server" style="width: 90%"></asp:TextBox><br /><br />

                    <asp:Label ID="Label4" runat="server" Text="Tags:"></asp:Label><br />
                    <asp:TextBox ID="txtTags" runat="server" style="width: 90%"></asp:TextBox><br /><br />

                    <asp:Label ID="Label5" runat="server" Text="Tech Level:"></asp:Label><br />
                    <asp:TextBox ID="txtTechLevel" runat="server" style="width: 90%"></asp:TextBox><br /><br />

                    <asp:Label ID="Label6" runat="server" Text="Atmosphere:"></asp:Label><br />
                    <asp:TextBox ID="txtAtmosphere" runat="server" style="width: 90%"></asp:TextBox><br /><br />

                    <asp:Label ID="Label7" runat="server" Text="Temperature:"></asp:Label><br />
                    <asp:TextBox ID="txtTemperature" runat="server" style="width: 90%"></asp:TextBox><br /><br />

                    <asp:Label ID="Label8" runat="server" Text="Biosphere:"></asp:Label><br />
                    <asp:TextBox ID="txtBiosphere" runat="server" style="width: 90%"></asp:TextBox>
                </div>
            <%--</ContentTemplate>
        </asp:UpdatePanel>

        <asp:UpdatePanel ID="updCulture" runat="server">
            <ContentTemplate>--%>
                <div class="col-md-4 col-sm-6 col-xs-12">
                    <asp:Label ID="Label9" runat="server" Text="Capital and Government:"></asp:Label><br />
                    <asp:TextBox ID="txtCapitalAndGovernment" runat="server" 
                        style="width: 90%; min-height:300px" TextMode="MultiLine"></asp:TextBox><br /><br />

                    <asp:Label ID="Label10" runat="server" Text="Cultural Notes:"></asp:Label><br />
                    <asp:TextBox ID="txtCulturalNotes" runat="server" 
                        style="width: 90%; min-height:300px" TextMode="MultiLine"></asp:TextBox>
                </div>
            <%--</ContentTemplate>
        </asp:UpdatePanel>
    <!--</div>

    <div class="row">-->
        <asp:UpdatePanel ID="updFriendsAndEnemies" runat="server">
            <ContentTemplate>--%>
                <div class="col-md-4 col-sm-6 col-xs-12">
                    <asp:Label ID="Label11" runat="server" Text="Friends:"></asp:Label><br />
                    <asp:TextBox ID="txtFriends" runat="server" 
                        style="width: 90%; min-height:300px" TextMode="MultiLine"></asp:TextBox><br /><br />

                    <asp:Label ID="Label12" runat="server" Text="Enemies:"></asp:Label><br />
                    <asp:TextBox ID="txtEnemies" runat="server" 
                        style="width: 90%; min-height:300px" TextMode="MultiLine"></asp:TextBox>
                </div>
            <%--</ContentTemplate>
        </asp:UpdatePanel>

        <asp:UpdatePanel ID="updEnvironment" runat="server">
            <ContentTemplate>--%>
                <div class="col-md-4 col-sm-6 col-xs-12">
                    <asp:Label ID="Label13" runat="server" Text="Places:"></asp:Label><br />
                    <asp:TextBox ID="txtPlaces" runat="server" 
                        style="width: 90%; min-height:300px" TextMode="MultiLine"></asp:TextBox><br /><br />

                    <asp:Label ID="Label14" runat="server" Text="Things:"></asp:Label><br />
                    <asp:TextBox ID="txtThings" runat="server" 
                        style="width: 90%; min-height:300px" TextMode="MultiLine"></asp:TextBox><br /><br />

                    <asp:Label ID="Label15" runat="server" Text="Complications:"></asp:Label><br />
                    <asp:TextBox ID="txtComplications" runat="server" 
                        style="width: 90%; min-height:300px" TextMode="MultiLine"></asp:TextBox>
                </div>
            <%--</ContentTemplate>
        </asp:UpdatePanel>

        <asp:UpdatePanel ID="updGMNotes" runat="server">
            <ContentTemplate>--%>
                <div class="col-md-4 col-sm-6 col-xs-12">
                    <asp:Label ID="Label16" runat="server" Text="Party Activities:"></asp:Label><br />
                    <asp:TextBox ID="txtPartyActivities" runat="server" 
                        style="width: 90%; min-height:300px" TextMode="MultiLine"></asp:TextBox><br /><br />

                    <asp:Label ID="Label17" runat="server" Text="Notes:"></asp:Label><br />
                    <asp:TextBox ID="txtNotes" runat="server" 
                        style="width: 90%; min-height:300px" TextMode="MultiLine"></asp:TextBox>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
</asp:Content>
