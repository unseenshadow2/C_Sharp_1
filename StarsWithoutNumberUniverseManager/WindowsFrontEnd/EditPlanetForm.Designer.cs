﻿namespace WindowsFrontEnd
{
    partial class EditPlanetForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lbxPlanets = new System.Windows.Forms.ListBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.txtName = new System.Windows.Forms.TextBox();
            this.txtTags = new System.Windows.Forms.TextBox();
            this.txtTechLevel = new System.Windows.Forms.TextBox();
            this.txtAtmosphere = new System.Windows.Forms.TextBox();
            this.txtTemperature = new System.Windows.Forms.TextBox();
            this.txtBiosphere = new System.Windows.Forms.TextBox();
            this.btnSave = new System.Windows.Forms.Button();
            this.txtLocation = new System.Windows.Forms.MaskedTextBox();
            this.txtNotes = new System.Windows.Forms.TextBox();
            this.txtCulturalNotes = new System.Windows.Forms.TextBox();
            this.txtCapitalAndGovernment = new System.Windows.Forms.TextBox();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.txtPartyActivities = new System.Windows.Forms.TextBox();
            this.txtPlaces = new System.Windows.Forms.TextBox();
            this.txtFriends = new System.Windows.Forms.TextBox();
            this.txtEnemies = new System.Windows.Forms.TextBox();
            this.txtThings = new System.Windows.Forms.TextBox();
            this.txtComplications = new System.Windows.Forms.TextBox();
            this.btnDelete = new System.Windows.Forms.Button();
            this.btnNew = new System.Windows.Forms.Button();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.SuspendLayout();
            // 
            // lbxPlanets
            // 
            this.lbxPlanets.FormattingEnabled = true;
            this.lbxPlanets.ItemHeight = 16;
            this.lbxPlanets.Location = new System.Drawing.Point(12, 37);
            this.lbxPlanets.Name = "lbxPlanets";
            this.lbxPlanets.Size = new System.Drawing.Size(218, 676);
            this.lbxPlanets.TabIndex = 0;
            this.lbxPlanets.SelectedIndexChanged += new System.EventHandler(this.lbxPlanets_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 14);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(59, 17);
            this.label1.TabIndex = 1;
            this.label1.Text = "Planets:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 3);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(66, 17);
            this.label2.TabIndex = 2;
            this.label2.Text = "Location:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 10);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(59, 17);
            this.label3.TabIndex = 3;
            this.label3.Text = "Friends:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(6, 334);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(66, 17);
            this.label4.TabIndex = 4;
            this.label4.Text = "Enemies:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(6, 54);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(49, 17);
            this.label5.TabIndex = 5;
            this.label5.Text = "Name:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(327, 10);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(54, 17);
            this.label6.TabIndex = 9;
            this.label6.Text = "Places:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(327, 334);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(55, 17);
            this.label7.TabIndex = 8;
            this.label7.Text = "Things:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(332, 27);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(165, 17);
            this.label8.TabIndex = 7;
            this.label8.Text = "Capital and Government:";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(651, 10);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(99, 17);
            this.label9.TabIndex = 6;
            this.label9.Text = "Complications:";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(656, 27);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(104, 17);
            this.label10.TabIndex = 13;
            this.label10.Text = "Party Activities:";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(656, 355);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(49, 17);
            this.label11.TabIndex = 12;
            this.label11.Text = "Notes:";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(6, 105);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(44, 17);
            this.label12.TabIndex = 11;
            this.label12.Text = "Tags:";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(332, 355);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(101, 17);
            this.label13.TabIndex = 10;
            this.label13.Text = "Cultural Notes:";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(6, 207);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(88, 17);
            this.label14.TabIndex = 17;
            this.label14.Text = "Atmosphere:";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(6, 258);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(94, 17);
            this.label15.TabIndex = 16;
            this.label15.Text = "Temperature:";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(6, 309);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(76, 17);
            this.label16.TabIndex = 15;
            this.label16.Text = "Biosphere:";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(6, 156);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(82, 17);
            this.label17.TabIndex = 14;
            this.label17.Text = "Tech Level:";
            // 
            // txtName
            // 
            this.txtName.Location = new System.Drawing.Point(9, 74);
            this.txtName.Name = "txtName";
            this.txtName.Size = new System.Drawing.Size(200, 22);
            this.txtName.TabIndex = 18;
            // 
            // txtTags
            // 
            this.txtTags.Location = new System.Drawing.Point(9, 125);
            this.txtTags.Name = "txtTags";
            this.txtTags.Size = new System.Drawing.Size(200, 22);
            this.txtTags.TabIndex = 19;
            // 
            // txtTechLevel
            // 
            this.txtTechLevel.Location = new System.Drawing.Point(9, 176);
            this.txtTechLevel.Name = "txtTechLevel";
            this.txtTechLevel.Size = new System.Drawing.Size(200, 22);
            this.txtTechLevel.TabIndex = 20;
            // 
            // txtAtmosphere
            // 
            this.txtAtmosphere.Location = new System.Drawing.Point(9, 227);
            this.txtAtmosphere.Name = "txtAtmosphere";
            this.txtAtmosphere.Size = new System.Drawing.Size(200, 22);
            this.txtAtmosphere.TabIndex = 21;
            // 
            // txtTemperature
            // 
            this.txtTemperature.Location = new System.Drawing.Point(9, 278);
            this.txtTemperature.Name = "txtTemperature";
            this.txtTemperature.Size = new System.Drawing.Size(200, 22);
            this.txtTemperature.TabIndex = 22;
            // 
            // txtBiosphere
            // 
            this.txtBiosphere.Location = new System.Drawing.Point(9, 329);
            this.txtBiosphere.Name = "txtBiosphere";
            this.txtBiosphere.Size = new System.Drawing.Size(200, 22);
            this.txtBiosphere.TabIndex = 23;
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(155, 718);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 23);
            this.btnSave.TabIndex = 25;
            this.btnSave.Text = "Save";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // txtLocation
            // 
            this.txtLocation.Location = new System.Drawing.Point(9, 24);
            this.txtLocation.Mask = "0000";
            this.txtLocation.Name = "txtLocation";
            this.txtLocation.Size = new System.Drawing.Size(63, 22);
            this.txtLocation.TabIndex = 26;
            this.txtLocation.ValidatingType = typeof(int);
            // 
            // txtNotes
            // 
            this.txtNotes.Location = new System.Drawing.Point(659, 375);
            this.txtNotes.Multiline = true;
            this.txtNotes.Name = "txtNotes";
            this.txtNotes.Size = new System.Drawing.Size(300, 300);
            this.txtNotes.TabIndex = 27;
            // 
            // txtCulturalNotes
            // 
            this.txtCulturalNotes.Location = new System.Drawing.Point(335, 375);
            this.txtCulturalNotes.Multiline = true;
            this.txtCulturalNotes.Name = "txtCulturalNotes";
            this.txtCulturalNotes.Size = new System.Drawing.Size(300, 300);
            this.txtCulturalNotes.TabIndex = 28;
            // 
            // txtCapitalAndGovernment
            // 
            this.txtCapitalAndGovernment.Location = new System.Drawing.Point(335, 51);
            this.txtCapitalAndGovernment.Multiline = true;
            this.txtCapitalAndGovernment.Name = "txtCapitalAndGovernment";
            this.txtCapitalAndGovernment.Size = new System.Drawing.Size(300, 300);
            this.txtCapitalAndGovernment.TabIndex = 29;
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Location = new System.Drawing.Point(236, 12);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(984, 739);
            this.tabControl1.TabIndex = 30;
            // 
            // tabPage1
            // 
            this.tabPage1.BackColor = System.Drawing.SystemColors.Control;
            this.tabPage1.Controls.Add(this.txtPartyActivities);
            this.tabPage1.Controls.Add(this.label2);
            this.tabPage1.Controls.Add(this.label11);
            this.tabPage1.Controls.Add(this.label13);
            this.tabPage1.Controls.Add(this.txtCapitalAndGovernment);
            this.tabPage1.Controls.Add(this.txtCulturalNotes);
            this.tabPage1.Controls.Add(this.txtNotes);
            this.tabPage1.Controls.Add(this.label10);
            this.tabPage1.Controls.Add(this.label5);
            this.tabPage1.Controls.Add(this.txtLocation);
            this.tabPage1.Controls.Add(this.label8);
            this.tabPage1.Controls.Add(this.label12);
            this.tabPage1.Controls.Add(this.label17);
            this.tabPage1.Controls.Add(this.txtBiosphere);
            this.tabPage1.Controls.Add(this.label16);
            this.tabPage1.Controls.Add(this.txtTemperature);
            this.tabPage1.Controls.Add(this.label15);
            this.tabPage1.Controls.Add(this.txtAtmosphere);
            this.tabPage1.Controls.Add(this.label14);
            this.tabPage1.Controls.Add(this.txtTechLevel);
            this.tabPage1.Controls.Add(this.txtName);
            this.tabPage1.Controls.Add(this.txtTags);
            this.tabPage1.Location = new System.Drawing.Point(4, 25);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(976, 710);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Planet Page 1";
            // 
            // tabPage2
            // 
            this.tabPage2.BackColor = System.Drawing.SystemColors.Control;
            this.tabPage2.Controls.Add(this.txtComplications);
            this.tabPage2.Controls.Add(this.txtPlaces);
            this.tabPage2.Controls.Add(this.txtFriends);
            this.tabPage2.Controls.Add(this.label9);
            this.tabPage2.Controls.Add(this.txtEnemies);
            this.tabPage2.Controls.Add(this.label7);
            this.tabPage2.Controls.Add(this.label6);
            this.tabPage2.Controls.Add(this.label3);
            this.tabPage2.Controls.Add(this.label4);
            this.tabPage2.Controls.Add(this.txtThings);
            this.tabPage2.Location = new System.Drawing.Point(4, 25);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(976, 710);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Planet Page 2";
            // 
            // txtPartyActivities
            // 
            this.txtPartyActivities.Location = new System.Drawing.Point(659, 51);
            this.txtPartyActivities.Multiline = true;
            this.txtPartyActivities.Name = "txtPartyActivities";
            this.txtPartyActivities.Size = new System.Drawing.Size(300, 300);
            this.txtPartyActivities.TabIndex = 30;
            // 
            // txtPlaces
            // 
            this.txtPlaces.Location = new System.Drawing.Point(330, 30);
            this.txtPlaces.Multiline = true;
            this.txtPlaces.Name = "txtPlaces";
            this.txtPlaces.Size = new System.Drawing.Size(300, 300);
            this.txtPlaces.TabIndex = 34;
            // 
            // txtFriends
            // 
            this.txtFriends.Location = new System.Drawing.Point(6, 30);
            this.txtFriends.Multiline = true;
            this.txtFriends.Name = "txtFriends";
            this.txtFriends.Size = new System.Drawing.Size(300, 300);
            this.txtFriends.TabIndex = 33;
            // 
            // txtEnemies
            // 
            this.txtEnemies.Location = new System.Drawing.Point(6, 354);
            this.txtEnemies.Multiline = true;
            this.txtEnemies.Name = "txtEnemies";
            this.txtEnemies.Size = new System.Drawing.Size(300, 300);
            this.txtEnemies.TabIndex = 32;
            // 
            // txtThings
            // 
            this.txtThings.Location = new System.Drawing.Point(330, 354);
            this.txtThings.Multiline = true;
            this.txtThings.Name = "txtThings";
            this.txtThings.Size = new System.Drawing.Size(300, 300);
            this.txtThings.TabIndex = 31;
            // 
            // txtComplications
            // 
            this.txtComplications.Location = new System.Drawing.Point(654, 30);
            this.txtComplications.Multiline = true;
            this.txtComplications.Name = "txtComplications";
            this.txtComplications.Size = new System.Drawing.Size(300, 300);
            this.txtComplications.TabIndex = 35;
            // 
            // btnDelete
            // 
            this.btnDelete.Location = new System.Drawing.Point(12, 718);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(75, 23);
            this.btnDelete.TabIndex = 31;
            this.btnDelete.Text = "Delete";
            this.btnDelete.UseVisualStyleBackColor = true;
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // btnNew
            // 
            this.btnNew.Location = new System.Drawing.Point(155, 11);
            this.btnNew.Name = "btnNew";
            this.btnNew.Size = new System.Drawing.Size(75, 23);
            this.btnNew.TabIndex = 32;
            this.btnNew.Text = "New";
            this.btnNew.UseVisualStyleBackColor = true;
            this.btnNew.Click += new System.EventHandler(this.btnNew_Click);
            // 
            // EditPlanetForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1232, 753);
            this.Controls.Add(this.btnNew);
            this.Controls.Add(this.btnDelete);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.lbxPlanets);
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "EditPlanetForm";
            this.Text = "Stars Without Number Planet Manager";
            this.Load += new System.EventHandler(this.EditPlanetForm_Load);
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListBox lbxPlanets;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox txtName;
        private System.Windows.Forms.TextBox txtTags;
        private System.Windows.Forms.TextBox txtTechLevel;
        private System.Windows.Forms.TextBox txtAtmosphere;
        private System.Windows.Forms.TextBox txtTemperature;
        private System.Windows.Forms.TextBox txtBiosphere;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.MaskedTextBox txtLocation;
        private System.Windows.Forms.TextBox txtNotes;
        private System.Windows.Forms.TextBox txtCulturalNotes;
        private System.Windows.Forms.TextBox txtCapitalAndGovernment;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.TextBox txtPartyActivities;
        private System.Windows.Forms.TextBox txtComplications;
        private System.Windows.Forms.TextBox txtPlaces;
        private System.Windows.Forms.TextBox txtFriends;
        private System.Windows.Forms.TextBox txtEnemies;
        private System.Windows.Forms.TextBox txtThings;
        private System.Windows.Forms.Button btnDelete;
        private System.Windows.Forms.Button btnNew;
    }
}

