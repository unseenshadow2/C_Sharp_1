﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFrontEnd
{
    public partial class EditPlanetForm : Form
    {
        /// <summary>
        /// Clear all the textboxes text values.
        /// </summary>
        private void ClearTextboxes()
        {
            txtLocation.Text = "";
            txtName.Text = "";
            txtFriends.Text = "";
            txtEnemies.Text = "";
            txtComplications.Text = "";
            txtPlaces.Text = "";
            txtThings.Text = "";
            txtCapitalAndGovernment.Text = "";
            txtCulturalNotes.Text = "";
            txtPartyActivities.Text = "";
            txtNotes.Text = "";
            txtTags.Text = "";
            txtTechLevel.Text = "";
            txtAtmosphere.Text = "";
            txtTemperature.Text = "";
            txtBiosphere.Text = "";
        }
    }
}
