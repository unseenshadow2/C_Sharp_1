﻿using RecordsSQL;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using RecordsInterface;

namespace WindowsFrontEnd
{
    /// <summary>
    /// This is the planet editing form.
    /// </summary>
    public partial class EditPlanetForm : Form
    {
        // The id of the current planet
        private int currentId = 0;

        public EditPlanetForm()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Set up the listbox when the form loads in.
        /// </summary>
        /// <param name="sender">The EditPlanetForm loading in.</param>
        /// <param name="e">Event arguments.</param>
        private void EditPlanetForm_Load(object sender, EventArgs e)
        {
            // Make sure that we only display the name of the planet in the
            // listbox
            lbxPlanets.DisplayMember = "name";
            updateListbox();
        }

        /// <summary>
        /// When the save button is pressed, save the current planet.
        /// </summary>
        /// <param name="sender">The save button.</param>
        /// <param name="e">Event arguments.</param>
        private void btnSave_Click(object sender, EventArgs e)
        {
            planet toSave = ToPlanet();

            if (PlanetsInterface.IsPlanetValid(toSave))
            {
                // If we aren't a new planet or we are a valid new planet, update
                if ((currentId != 0) || PlanetsInterface.IsNewPlanetValid(toSave))
                {
                    PlanetsInterface.UpdatePlanet(toSave);
                    currentId = toSave.id;
                    updateListbox();
                }
                else MessageBox.Show("New planet is not valid. Make sure it has a unique name.");
            }
            else MessageBox.Show("Planet is not valid. Make sure it has a location and a name.");
        }

        /// <summary>
        /// Clean everything up to ready for the creation of a new planet.
        /// </summary>
        /// <param name="sender">The new button.</param>
        /// <param name="e">Event Arguments.</param>
        private void btnNew_Click(object sender, EventArgs e)
        {
            lbxPlanets.ClearSelected();
            currentId = 0;
            ClearTextboxes();
        }

        /// <summary>
        /// Delete the current planet.
        /// </summary>
        /// <param name="sender">The delete button.</param>
        /// <param name="e">Event Arguments.</param>
        private void btnDelete_Click(object sender, EventArgs e)
        {
            if (currentId != 0) // Can't delete what don't exist.
            {
                PlanetsInterface.DeletePlanet(currentId);
                currentId = 0; // Clear current id
                updateListbox();
                lbxPlanets.SelectedIndex = 0; // Select item 0
            }
        }

        /// <summary>
        /// Pull down the values from the database and store the names and ids in
        /// the listbox.
        /// </summary>
        private void updateListbox()
        {
            // Clear and refill the listbox
            lbxPlanets.Items.Clear();
            lbxPlanets.Items.AddRange(PlanetsInterface.GetAllPlanets());

            // Make sure we have the right value selected in the listbox
            // after clearing the listbox
            if (currentId == 0) lbxPlanets.ClearSelected();
            else
            {
                planet query = (from planet p in lbxPlanets.Items
                                where (currentId == p.id)
                                select p).FirstOrDefault();

                lbxPlanets.SelectedItem = query;
            }
        }

        /// <summary>
        /// Update the current planet when we change the selected index.
        /// </summary>
        /// <param name="sender">The listbox.</param>
        /// <param name="e">Event Arguments.</param>
        private void lbxPlanets_SelectedIndexChanged(object sender, EventArgs e)
        {
            planet selected = lbxPlanets.SelectedItem as planet;
            if (selected != null) LoadFromPlanet(selected);
        }
    }
}
