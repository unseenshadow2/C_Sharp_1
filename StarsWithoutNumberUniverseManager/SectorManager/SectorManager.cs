﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using StarsRecords;
using Newtonsoft.Json;
using System.IO;
using RecordsManagers;

namespace DataManagers
{
    public class SectorManager
    {
        public Sector sector = new Sector();

        /// <summary>
        /// Save the sector to the disk.
        /// </summary>
        /// <param name="filepath">The filepath to the output file</param>
        public void SaveSector(string filepath)
        {
            File.WriteAllText(filepath, JsonConvert.SerializeObject(sector, Formatting.Indented));
        }

        /// <summary>
        /// Load the sector from the disk.
        /// </summary>
        /// <param name="filepath">The filepath to the input file</param>
        public void LoadSector(string filepath)
        {
            sector = JsonConvert.DeserializeObject<Sector>(File.ReadAllText(filepath));
        }
    }
}
