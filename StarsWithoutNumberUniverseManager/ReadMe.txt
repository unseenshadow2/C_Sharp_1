#############################
##### Basic Information #####
#############################

Project Name: Stars Without Number Planet Manager
Programmer: Anthony Parsch
Recommended Materials:
	Stars Without Number Core Book (Free or Paid Edition)

Unused Libraries:
	StarsRecords - May be kept for future upgrades.
	EnumerationClass - A useful tool that may be used in later versions.
	RandomGeneration - Definitely going to be kept for future use.

################################
##### Instructions - Setup #####
################################

The following instructions cover the initial setup of the SQL database:

1.	Install Microsoft SQL Server (https://www.microsoft.com/en-cy/sql-server/developer-tools)
2.	Install Visual Studio 2017 (https://www.visualstudio.com/downloads/)
	2a.	Open Visual Studio 2017, click on File/Open/"Project/Solution" then go to the location
		of "StarsWithoutNumberUniverseManager.sln" and open the file.
	2b.	Open the location of the "StarsWithoutNumberUniverseManager.sln" in Windows Explorer 
		and double click said file. The file should open in Visual Studio.
3.	Open Microsoft SQL Management Studio
	3a.	If MSMS was not install correctly, open the MS SQL Server and install it from there
4.	Open Microsoft SQL Management Studio and connect to a local database
5.	Open a new query in SQL Management Studio
6.	Copy the contents of database.txt and paste them into the query
7.	Run the query
8.	Return to Visual Studio
9.	Open the "Server Explorer" tab (View/Server Explorer)
10.	Right click "Data Connections" and click add connection
	10b.	Follow the wizard to connect the database you made in step 4 to Visual Studio
11.	Update the connection strings in "WindowsFrontEnd/App.config" and TODO:Name of web front end
	(https://stackoverflow.com/questions/5299775/how-should-i-edit-an-entity-framework-connection-string)
	11a.	A connection string can be obtained by selecting the database in the Server Explorer
		and right clicking it, then clicking Properties. Within the Properties window, go to
		the "Connection String" property and click into the right side box. Then, right click
		and click "Select All" and right click again and click "Copy." You can now paste the
		connection string with either CTRL-V or right clicking and clicking "Paste."

##########################################
##### Instructions - WindowsFrontEnd #####
##########################################

The following instructions cover how to compile and use the WindowsFrontEnd
project included in this solution.

##### Compiling #####
1.	Open the solution in Visual Studio (if reading this file outside of Visual Studio). 
	1a.	See "Instructions - Setup" steps 2a and 2c.
2.	Open the "PAL" folder in the Solution Explorer
3.	Right click "WindowsFrontEnd" and click "Set as StartUp Project"
4.	Right click "WindowsFrontEnd" again and click "Rebuild"
	4a.	If you get messages about missing reference or packages, use NuGet to update/install
		"Entity Framework" (https://docs.microsoft.com/en-us/nuget/consume-packages/package-restore)
5.	Click the "Start" button in the toolbar.
	5a. Or go to the "Debug" menu and click on "Start Debugging" or "Start Without Debugging"
	5b.	Or press F5 for starting with debugging or CTRL-F5 to start without debugging

##### Using #####
### Planets Listbox ###
The Planets Listbox is the big listbox off to the left. This will fill with all the planets in
the "planets" table of the database you connected to the program. You can select a planet to
view by clicking on the name of the planet in the listbox. If you don't see any and everything
else worked properly up to this point, you most likely just need to make planets.

### Planet Viewer ###
The Planet Viewer is the majority of the program, taking up the middle and the right sections.
At the top is two tabs which control what information of the planet is displaying. Any of the
textboxes within the Planet Viewer can be edited at any time, however a planet will NOT save if
it doesn't have a unique name or a location that is four digits (0-9) long. The typical location
string for Stars Without Number is in the following format: 0x0y, where x is the row and y is the
column. If you are preparing for a game with more than one sector, you can make the two zeros
into the sector coordinates.

### The Buttons ###
There are three buttons in the Planet Manager, all of which surround the Planets Listbox.

Up on the top right is the New button. This button will reset the Planet Viewer to make a
new planet.

On the bottom right is the Save button. This button will save the current planet in the
Planet Viewer to the database. If the planet is new, it will add it. If the planet already
exists, it will update it.

######################################
##### Instructions - WebFrontEnd #####
######################################

The following instructions cover how to compile and use the WebFrontEnd
project included in this solution.

##### Compiling #####
1.	Open the solution in Visual Studio (if reading this file outside of Visual Studio). 
	1a.	See "Instructions - Setup" steps 2a and 2c.
2.	Open the "PAL" folder in the Solution Explorer
3.	Right click "WebFrontEnd" and click "Set as StartUp Project"
4.	Click the green arrow button in the toolbar.
	4a.	If you get messages about missing reference or packages, use NuGet to update/install
		"Entity Framework" (https://docs.microsoft.com/en-us/nuget/consume-packages/package-restore)
	4b.	You can change the web browser that opens the project by clicking the drop down arrow
		slightly to the right of the green arrow button

##### Using ######
A general usage issue: While the web page does have validation, it lacks the ability to 
alert the user. If you run into an issue with a planet not being created or saved, ensure
that the location and name are both valid.

### Planets Listbox ###
The Planets Listbox is the listbox at the top left or top center of the web page. The
Planets Listbox contains the names of all accessable planets. Single clicking will
select a planet, so that the planet can be used by the Load button or the Delete
button.

### The Four Buttons ###
Directly below the Planets Listbox are four buttons: New, Load, Save, and Delete.
The names of the buttons are self explanatory. The use of the four buttons is covered
below:

** New
The New button clears all the textboxes in the web page and prepares the backend to save
a new planet.

** Load
The Load button loads a planet into the textboxes and prepares the backend to update its
entry. Load requires that a planet be selected in the Planets Listbox.

** Save
The Save button saves the current planet. If the planet is new (no planet has been loaded
or the new button was used), it will save the planet as a new entry to the database. If
a planet has been loaded into the web page, then the Save button updates the database
entry.

In order for a planet to be saved, it must meet two requirements:
1. The planet must have a four digit (0-9) location code
2. The planet must have a unique name

** Delete
The Delete button deletes an entry from the database. The Delete button requires that a 
planet be selected in the Planets Listbox.

### Textboxes ###
A majority of the web page is taken up by the textboxes. These textboxes contain all the
data from the current planet. When a new planet is loaded, all the textboxes will be filled
with the information of the new planet. When a planet is saved, all of the information from
these textboxes is sent to the database.